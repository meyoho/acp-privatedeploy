#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import getopt
from acp_privatepack.autopackage import *

if __name__ == '__main__':
    install_asm = 'zz'
    install_amp = 'zz'
    install_aml = 'zz'
    install_devops = 'zz'
    install_ace3 = 'zz'
    package_asm = 'zz'
    package_amp = 'zz'
    package_aml = 'zz'
    package_csp = 'zz'
    package_acp = 'zz'
    package_devops = 'zz'
    package_tdsql = 'zz'
    package_timatrix = 'zz'
    package_tsf = 'zz'
    use_http = 'zz'
    use_domain_name = 'zz'
    packdeploy = ''
    package_message = ''
    enabled_branch = 'acp2'
    chart_version = 'zz'
    arm_package = 'zz'
## 如果没有 -f 或 --enabled-features 参数，默认从 release 的 chart repo 下载 chart 打包，默认不部署环境
    enabled_features = ''
## 如果没有 -p 或 --package-list 参数，默认打包 acp 和 asm
    package_list = 'acp,asm'
## 获取参数
    opts,args = getopt.getopt(sys.argv[1:],'-p:-f:-v:-b:',['enabled-features=','package-list=','enabled-branch=','chart-version=','arm'])
    for opt_name,opt_value in opts:
        if opt_name in ('-p','--package-list'):
## 如果有参数 -p 或 --package-list ，定义 package_list 变量，确认打包那些产品
            package_list = opt_value
            print("package list is ", package_list)
            r_message = '-p 或 --package-list 参数的值是：' + opt_value
            send_message(r_message)
        if opt_name in ('-f','--enabled-features'):
## 如果有参数 -f 或 --enabled-features ， 定义 enabled_features ，确认从那个 chart repo 下载 chart 打包，确认是否部署环境，部署那些产品，那种类型的环境
            enabled_features = opt_value
            print("features is ", enabled_features)
            r_message = '-f 或 --enabled-features 参数的值是：' + opt_value
            send_message(r_message)
        if opt_name in ('-b','--enabled-branch'):
## 如果有参数 -b 或 --enabled-branch ， 定义 enabled_branch ，确认从那个分支 clone newprivatedeploy 仓库，默认 acp2
            enabled_branch = opt_value
            print("branch is ", enabled_branch)
            r_message = '-b 或 --enabled-branch 参数的值是：' + opt_value
            send_message(r_message)
        if opt_name in ('-v','--chart-version'):
## 如果有参数 -v 或 --chart-version ， 定义 chart_version ，确认从下次 chart repo 中那个版本，如果没有这个参数，默认最新的 chart 版本
            chart_version = opt_value
            print("chart version is ", chart_version)
            r_message = '-v 或 --chart-version 参数的值是：' + opt_value
            send_message(r_message)
        if opt_name in ('--arm'):
## 如果有参数 --arm ， 定义 arm_package ，确认打的是 arm 的包，从armarmharbor.alauda.cn 下镜像
            arm_package = 'package'
            r_message = '发现 --arm 参数，要打 arm 的安装包'
            send_message(r_message)
    package_message = package_message + '将从 newprivatedeploy 仓库的 ' + enabled_branch + '分支获取部署代码。'
    if chart_version == 'zz':
        package_message = package_message + '将要打包最新版本的 chart 。'
    else:
        package_message = package_message + '将要打包的 chart 的版本是 ' + chart_version + '。'
    if 'every_day' in enabled_features:
        packdeploy = 'everyday'
        package_message = '将要部署一个环境，部署'
    elif 'auto_pack_deploy' in enabled_features:
        packdeploy = 'autopackdeploy'
        package_message = '将要部署一个环境，部署'


    if 'asm' in enabled_features:
        install_asm = 'true'
        package_message = package_message + ' asm ，'
    if 'amp' in enabled_features:
        install_amp = 'true'
        package_message = package_message + ' amp ，'
    if 'aml' in enabled_features:
        install_aml = 'true'
        package_message = package_message + ' aml ，'
    if 'devops' in enabled_features:
        install_devops = 'true'
        package_message = package_message + ' devops ，'
    if 'ace3' in enabled_features:
        install_ace3 = 'true'
        package_message = package_message + ' ace3 ，'
    if 'domain' in enabled_features:
        use_domain_name = 'true'
        package_message = package_message + '环境使用域名访问 ，'
    else:
        package_message = package_message + '环境使用 ip 访问 ，'
    if 'http' in enabled_features:
        use_http = 'true'
        package_message = package_message + '使用 http 协议访问 。'
    else:
        package_message = package_message + '使用 https 协议访问 。'


    package_message = package_message + '将要打如下产品的安装包： '
    if 'acp' in package_list:
        package_acp = 'true'
        package_message = package_message + 'acp, '
    if 'devops' in package_list:
        package_devops = 'true'
        package_message = package_message + 'devops, '
    if 'asm' in package_list:
        package_asm = 'true'
        package_message = package_message + 'asm, '
    if 'amp' in package_list:
        package_amp = 'true'
        package_message = package_message + 'amp, '
    if 'aml' in package_list:
        package_aml = 'true'
        package_message = package_message + 'aml, '
    if 'csp' in package_list:
        package_csp = 'true'
        package_message = package_message + 'csp, '
    if 'tdsql' in package_list:
        package_tdsql = 'true'
        package_message = package_message + 'tdsql, '
    if 'timatrix' in package_list:
        package_timatrix = 'true'
        package_message = package_message + 'ti-matrix, '
    if 'tsf' in package_list:
        package_tsf = 'true'
        package_message = package_message + 'tsf, '
    send_message(package_message)
#    print("install_tke=" + install_tke + "   use_domain_name=" + use_domain_name + "   use_http=" + use_http)
    package(packdeploy, use_http, use_domain_name, install_asm, install_amp, install_aml, install_devops, package_acp, package_devops, package_asm, package_amp, package_csp, package_aml, package_tdsql, package_timatrix, package_tsf, install_ace3, enabled_branch, chart_version, arm_package)
