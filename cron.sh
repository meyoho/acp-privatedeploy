#!/bin/bash

WORKROOT=`cd $(dirname $0); pwd -P`

cd  $WORKROOT && pipenv install
pipenv run python $WORKROOT/private_release.py

