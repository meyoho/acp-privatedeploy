#!/usr/bin/python
# -*- coding: utf-8 -*-
from acp_privatetest.privatetest import deployandtest
from acp_privatetest.common_func import assert_successful
from acp_privatetest.qcloud import delete_instance
import settings
import time

if __name__ == '__main__':
    test_need_info = dict()
    with open(settings.WORKHOME + '/qa-info-' + time.strftime('%Y%m%d') + '.json', 'r') as fp:
        test_need_info = eval(fp.read())
    print("test need info  is {}".format(test_need_info))
    results = deployandtest(test_need_info)
    if results["success"]:
        print("开始删除腾讯机器")
        result = delete_instance()
        assert_successful(result)
        print("删除完毕，QA部分完成")
        print("新包地址:{}".format(test_need_info["package_path"]))
    else:
        print("测试失败，请查看测试结果:{}，并执行脚本删除机器:pipenv run  python del_vm.py".format(results))
