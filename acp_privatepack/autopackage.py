# -*- coding: utf-8 -*-
import settings
import sys
import os
import shutil
import time
import subprocess
import gevent
from acp_privatepack.common_func import *
from acp_privatepack.images_list import *
from acp_privatepack.chart_list import *
from settings import *

for i in PACK_ROOT, WORK_ROOT, CHARTS_DIR, REGISTRY_DIR, TEMP_DIR, CHARTSTMP_DIR:
    subprocess.check_call('mkdir -p ' + i, shell=True)
def package(packdeploy, use_http, use_domain_name, install_asm, install_amp, install_aml, install_devops, package_acp, package_devops, package_asm, package_amp, package_csp, package_aml, package_tdsql, package_timatrix, package_tsf, install_ace3, enabled_branch, chart_version, arm_package):
    if packdeploy == 'everyday':
        settings.CHARTMUSEUM_URL = 'http://alauda-chart-stable-new.ace-default.cloud.myalauda.cn/'
        r_message = '每天自动打包、部署、测试，从 stable 仓库 pull chart'
        send_message(r_message)
    elif packdeploy == 'autopackdeploy':
        r_message = '发版，自动打包部署，从 release 仓库 pull chart'
        send_message(r_message)
    else:
        r_message = '发版，打包之后不自动部署，从 release 仓库 pull chart'
        send_message(r_message)
    r_message = '开始打包'
    send_message(r_message)
    r_message = '   我的工作目录在这里 ' + WORK_ROOT
    send_message(r_message)
    os.chdir(WORK_ROOT)
    session = auth_session()
    components_url = get_components_from_cm()
    send_message(r_message)
#    subprocess.call('git clone https://hongtaowang@bitbucket.org/mathildetech/newprivatedeploy.git --depth=1 -b acp2 /tmp/newprivatedeploy', shell=True)
    os.system("git clone https://hongtaowang@bitbucket.org/mathildetech/newprivatedeploy.git --depth=1 -b %s /tmp/newprivatedeploy" % (enabled_branch))

    r_message = '  克隆 newprivatedeploy 仓库 ' + enabled_branch + ' 分支的代码到 /tmp/newprivatedeploy 目录下'
    send_message(r_message)
    if chart_version == 'zz':
        for i in components_url:
            download_file(i, session, CHARTS_DIR)
        r_message = '  从ChartMuseum抓到了一堆组件，按版本号排序，获取最新版本的 chart'
    else:
        os.system("cd %s ; for i in $(helm search | grep ^rel | awk '{print $1}'); do helm fetch $i --version %s || helm fetch $i ; done" % (CHARTS_DIR, chart_version))
        r_message = '  从ChartMuseum抓到了一堆组件，获取版本号为 ' + chart_version + ' 的 chart，如果某个 chart 没有这个版本，就按版本号排序，获取最新版本的 chart'
    send_message(r_message)
    time.sleep(60)

# 打 amp 的安装包
    if package_amp == 'true':
        pack_small_products('amp', AMPLIST, arm_package)

# 打 asm 的安装包
    if package_asm == 'true':
        pack_small_products('asm', ASMLIST, arm_package)

# 打 aml 的安装包
    if package_aml == 'true':
        pack_small_products('aml', AMLLIST, arm_package)

# 打 tdsql 的安装包
    if package_tdsql == 'true':
        pack_small_products('tdsql', TDSQLLIST, arm_package)

# 打 csp  的安装包
    if package_csp == 'true':
        pack_small_products('csp', CSPLIST, arm_package)

# 打 ti-matrix 的安装包
    if package_timatrix == 'true':
        pack_small_products('timatrix', TILIST, arm_package)

# 打 tsf 的安装包
    if package_tsf == 'true':
        pack_small_products('tsf', TSFLIST, arm_package)

# 打 devops 的安装包
    if package_devops == 'true':
        pack_small_products('devops', DEVOPSLIST, arm_package)

# 打 acp 的安装包
    if package_acp == 'true':
        pack_small_products('acp', ACPLIST, arm_package, EXTRAIMAGELIST, ARMEXTRAIMAGELIST)

# 打包结束
    r_message = '  打包结束，所有安装包在 ' + PACK_ROOT + ' 这个目录下'
    send_message(r_message)

# 开始自动部署，自动测试
    acp_version = os.popen("helm repo up >/dev/null 2>&1 && helm search release/dex | awk 'END{print $2}' | sed -e 's/\./-/g'").read( )
    print(acp_version)
    if packdeploy == 'everyday':
        r_message = '每天自动打包、部署、测试，开始自动部署，部署一个单点的环境'
        send_message(r_message)
        os.system("bash /root/wanghongtao/auto_install_acp2-2.8.sh package_file=%s project=auto-deploy-check project_id=1126842 single=true" % (PACK_ROOT))
    elif packdeploy == 'autopackdeploy':
        r_message = '发版，自动打包部署'
        send_message(r_message)
        r_message = "部署命令：bash /root/wanghongtao/auto_install_acp2-2.8.sh project_id=1126842 deploy_region=true deploy_region_all=true use_domain_name=" + use_domain_name + " use_http=" + use_http + " package_file=" + PACK_ROOT + " project=aq-zy-" + acp_version + " install_asm=" + install_asm + " install_amp=" + install_amp + " install_aml=" + install_aml + " install_devops=" + install_devops + " install_ace3=" + install_ace3
        send_message(r_message) 
        os.system("echo bash /root/wanghongtao/auto_install_acp2-2.8.sh project_id=1126842 deploy_region=true deploy_region_all=true use_domain_name=%s use_http=%s package_file=%s install_asm=%s install_amp=%s install_aml=%s install_devops=%s install_ace3=%s project=aq-zy-%s " % (use_domain_name, use_http, PACK_ROOT, install_asm, install_amp, install_aml, install_devops, install_ace3, acp_version))
        os.system("     bash /root/wanghongtao/auto_install_acp2-2.8.sh project_id=1126842 deploy_region=true deploy_region_all=true use_domain_name=%s use_http=%s package_file=%s install_asm=%s install_amp=%s install_aml=%s install_devops=%s install_ace3=%s project=aq-zy-%s " % (use_domain_name, use_http, PACK_ROOT, install_asm, install_amp, install_aml, install_devops, install_ace3, acp_version)) 
