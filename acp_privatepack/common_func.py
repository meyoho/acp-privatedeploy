# -*- coding: utf-8 -*-
import tarfile
import requests
import yaml
import json
import os
import re
import time
import subprocess
import docker
import gevent
import settings
from settings import *


container = docker.from_env()

def send_message(r_message):
    r_message = '程序已经运行了： ' + calculate_how_much_time_has_passed(START_TIME) + '  ' + r_message
    print(r_message)
#    os.system("bash %s/send_message.sh '%s'" % (PACKWORKROOT, r_message))
    d = {"msgtype": "text","text": {"content": r_message}}
    res = requests.post(URL, data = json.dumps(d), headers = HEADERS)

def auth_session():
    session = requests.Session()
    session.auth = (settings.CHARTMUSEUM_USER, settings.CHARTMUSEUM_PASSWORD)
    return session

def get_components_from_cm():
    url = settings.CHARTMUSEUM_URL
    if url[-1] is not '/':
        url = url + '/'
    session = auth_session()
    request = session.get(url + 'index.yaml')
    components_list = list(yaml.load(request.text)['entries'].keys())
    def get_component_url(chart):
        j = json.loads(session.get(url + 'api/charts/' + chart).text)
        return  url + j[0]['urls'][0]
    jobs = [gevent.spawn(get_component_url, component) for component in components_list]
    gevent.joinall(jobs)
    return [job.value for job in jobs]

def download_file(url, session, dir):
    filename = url.split('/')[-1]
    if session is 'PASS':
        file = requests.get(url, stream=True)
    else:
        file = session.get(url, stream=True)
    with open(dir + os.sep + filename, 'wb') as f:
        f.write(file.content)

def get_images_from_yaml(yamlfile):
    with open(yamlfile, 'r') as f:
        y = yaml.load(f)
        temp = y['global']['images']
        def get_image(key):
            registry = y['global']['registry']['address']
            repo = y['global']['images'][key]['repository']
            tag = str(y['global']['images'][key]['tag'])
            return str(registry + '/' + repo + ':' + tag)
        jobs = [gevent.spawn(get_image, key) for key in temp.keys()]
        gevent.joinall(jobs)
    return [job.value for job in jobs]

def rm_container(container):
    checkcommand = 'docker ps -a | grep ' + container
    c = subprocess.Popen(checkcommand, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    c.wait()
    s = c.stdout.read()[0:7].decode('ascii')
    if s is not '':
        clean = 'docker rm -f ' + s
        p = subprocess.Popen(clean, shell=True)
        p.wait()

def create_registry(REGISTRY_DIR):
    rm_container('acp-registry')
    startregistrycommand = 'docker run -d --name acp-registry -p 25000:5000 -v ' + \
                           REGISTRY_DIR + ':/var/lib/registry registry && sleep 5'
    p = subprocess.Popen(startregistrycommand, shell=True)
    p.wait()
    r = requests.get('http://localhost:25000')
    return r.status_code

def create_chartmuseum(CHARTS_DIR):
    rm_container('acp-chartmuseum')
    startchartmuseumcommand = 'docker run -d --name=acp-chartmuseum -p 28000:8080 -e PORT=8080 -e DEBUG=1 -e STORAGE="local" -e STORAGE_LOCAL_ROOTDIR="/data" -e BASIC_AUTH_USER="chartmuseum" -e BASIC_AUTH_PASS="chartmuseum" -v ' + \
                              CHARTS_DIR + ':/data chartmuseum/chartmuseum && sleep 5'
    p = subprocess.Popen(startchartmuseumcommand, shell=True)
    p.wait()
    return requests.get('http://localhost:28000', auth=('chartmuseum', 'chartmuseum')).status_code

def pull_image(image):
    if ':' not in image:
        image = image + ':latest'
    try:
        container.pull(image)
    except Exception:
        print('拉取镜像失败: ' + image)
    else:
        print('拉取镜像成功: ' + image)

def push_image_to_local_registry(image):
    if len(re.findall('/', image)) is not 0:
        new_tag = '127.0.0.1:25000/' + '/'.join(image.split('/')[1:])
    else:
        new_tag = '127.0.0.1:25000/' + image
    docker_tag = subprocess.Popen(
        ['docker', 'tag', image, new_tag], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    error = docker_tag.stderr.readlines()
    if len(error) is not 0:
        print('docker tag 失败: ' + error[0].decode('ascii'))
    else:
        try:
            container.push(new_tag)
        except Exception:
            print('推送镜像失败: ' + new_tag)
        else:
            print('推送镜像成功: ' + new_tag)

def calculate_how_much_time_has_passed(startsecond):
    seconds = int(time.time() - startsecond)
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    s = str(s)
    m = str(m)
    h = str(h)
    r_time = h + '小时' + m + '分' + s + '秒'
    return r_time

def pack_small_products(p_name, charts_list, arm_package, extend_images_list = [], arm_extend_images_list = []):
    docker_images_list = []
    if arm_package == 'package':
        docker_images_list += arm_extend_images_list
    else:
        docker_images_list += extend_images_list
    os.system("rm -rf %s && mkdir %s ; rm -rf /tmp/acp-chart-tmp ; mkdir /tmp/acp-chart-tmp ; rm -rf %s && mkdir %s ; rm -rf %s/version; rm -rf %s && mkdir %s" % (REGISTRY_DIR, REGISTRY_DIR, CHARTSTMP_DIR, CHARTSTMP_DIR, WORK_ROOT, TEMP_DIR, TEMP_DIR))
    r_message = '  开始打 ' + p_name + ' 的安装包'
    send_message(r_message)
    for i in charts_list:
        os.system("cp %s/%s-*.tgz %s" % (CHARTS_DIR, i, CHARTSTMP_DIR))
    for i in os.listdir(CHARTSTMP_DIR):
        tar = tarfile.open(CHARTS_DIR + os.sep + i, 'r')
        tar.extractall(TEMP_DIR)
        tar.close()
    r_message = '  解压 ' + p_name + ' 相关的chart 备用'
    send_message(r_message)
    for i in os.listdir(TEMP_DIR):
        try:
            images_list = get_images_from_yaml(TEMP_DIR + os.sep + i + os.sep + 'values.yaml')
            images_list = [x for x in images_list if ":" in x]
            r_message = i + ' 这个 chart 中包含这些镜像： '
            print(r_message)
            print('\n'.join(map(str, images_list)))
            docker_images_list += images_list
        except:
            r_message = i.ljust(40) + '这个 chart 中的 values.yaml 格式不标准，解析不出来镜像'
            send_message(r_message)
            continue
    r_message = '  拉取从' + p_name + ' 的所有 Chart 里解析到镜像'
    send_message(r_message)
    if arm_package == 'package':
        r_message = '本地打包是 arm 版，将 harbor.alauda.cn 和 index.alauda.cn 这个两个仓库替换成 armharbor.alauda.cn'
        send_message(r_message)
        docker_images_list = [re.sub(r'^harbor.alauda.cn', 'armharbor.alauda.cn', c) for c in docker_images_list]
        docker_images_list = [c.replace('harbor-b.alauda.cn','armharbor.alauda.cn') for c in docker_images_list]
        docker_images_list = [c.replace('index.alauda.cn','armharbor.alauda.cn') for c in docker_images_list]

    pull = [gevent.spawn(pull_image, image) for image in docker_images_list]
    gevent.joinall(pull)
    r_message = '  启动本地仓库'
    send_message(r_message)
    if create_registry(REGISTRY_DIR) is not 200:
        r_message = '  启动 registry 失败'
        send_message(r_message)
        raise RuntimeError('create registry container fail.')
    r_message = '  把镜像推到' + p_name + ' 的镜像仓库里，请耐心等待'
    send_message(r_message)
    push = [gevent.spawn(push_image_to_local_registry, image) for image in docker_images_list]
    gevent.joinall(push)
#    subprocess.call(['docker', 'save', 'registry', '-o', WORK_ROOT + os.sep + 'registry.tar'])
    if arm_package == 'package':
        os.system("cp -Ra /mnt/package/wht/registry/registry-arm.tar %s/registry.tar" % (WORK_ROOT))
    else:
        os.system("cp -Ra /mnt/package/wht/registry/registry.tar %s/registry.tar" % (WORK_ROOT))
    r_message = '  开始打' + p_name + ' 的安装包'
    send_message(r_message)
    if p_name == 'acp':
        if arm_package == 'package':
            package_path = PACK_ROOT + '/cpaas-arm-' + time.strftime('%Y%m%d')
        else:
            package_path = PACK_ROOT + '/cpaas-' + time.strftime('%Y%m%d')
        do_something(docker_images_list, arm_package)
    else:
        if arm_package == 'package':
            package_path = PACK_ROOT + '/cpaas-arm-' + p_name + '-' + time.strftime('%Y%m%d')
        else:
            package_path = PACK_ROOT + '/cpaas-' + p_name + '-' + time.strftime('%Y%m%d')
        os.system("cp -Ra /tmp/newprivatedeploy/function.sh %s" % (WORK_ROOT))
        os.system("cp -Ra /tmp/newprivatedeploy/upload-images-chart.sh %s" % (WORK_ROOT))
    rm_container('registry')
    os.system("rm -rf %s/version" % (WORK_ROOT))
    with open(WORK_ROOT + '/version', 'w') as f:
        for i in docker_images_list:
            if isinstance(i,str):
                f.write(i + '\n')
    os.system("rm -rf %s" % (TEMP_DIR))
    os.chdir(PACK_ROOT)
    subprocess.call('tar --use-compress-program=pigz -cpf ' + package_path + '.tgz ' + './cpaas', shell=True)
    r_message = p_name + ' 的安装包已经打好，在 ' + package_path + '.tgz'
    send_message(r_message)

def do_something(docker_images_list, arm_package):
    os.chdir(WORK_ROOT)
#    subprocess.call('git clone https://hongtaowang@bitbucket.org/mathildetech/newprivatedeploy.git --depth=1 -b acp2', shell=True)
    subprocess.call('cp -ra /tmp/newprivatedeploy/* . && rm -rf newprivatedeploy', shell=True)
    subprocess.call('cp -Ra /mnt/package/wht/other .', shell=True)
    if arm_package == 'package':
        subprocess.call('rm -rf ./other/docker.tgz ; mv ./other/docker-linux-arm64-18.09.9.tar.gz ./other/docker.tgz', shell=True)
    else:
        subprocess.call('rm -rf ./other/docker-linux-arm64-18.09.9.tar.gz', shell=True)

# 下载 captain 的安装 yaml
    subprocess.call('curl --socks5 127.0.0.1:8888 https://raw.githubusercontent.com/alauda/captain/master/artifacts/all/release.yaml -o ./captain-deploy.yaml', shell=True)
# pull 从 yaml 文件中获取到的镜像，并 push 到本地仓库里
    subprocess.call('rm -rf /tmp/captain.images ; for i in $(cat ./captain-deploy.yaml | awk "/ image: /{print \$2}"); do echo $i >> /tmp/captain.images ; done', shell=True)

    if arm_package == 'package':
        r_message = '本地打包是 arm 版，将从 armharbor.alauda.cn 这个仓库获取 catpain 的镜像'
        send_message(r_message)
        subprocess.call('for i in $(cat /tmp/captain.images); do docker pull ${i/index.alauda.cn/armharbor.alauda.cn} && echo "拉取镜像成功: ${i/index.alauda.cn/armharbor.alauda.cn}" || echo "拉取镜像失败: ${i/index.alauda.cn/armharbor.alauda.cn}" ; docker tag ${i/index.alauda.cn/armharbor.alauda.cn} ${i/index.alauda.cn/127.0.0.1:25000} ; docker push ${i/index.alauda.cn/127.0.0.1:25000} ; done', shell=True)
    else:
        subprocess.call('for i in $(cat /tmp/captain.images); do docker pull $i && echo "拉取镜像成功: ${i}" || echo "拉取镜像失败: ${i}" ; docker tag $i ${i/index.alauda.cn/127.0.0.1:25000} ; docker push ${i/index.alauda.cn/127.0.0.1:25000} ; done', shell=True)

    r_message = '  拷贝 newprivatedeploy 代码仓库里的所有文件到安装包'
    send_message(r_message)
    subprocess.call('mv cleanup cleanup.sh && chmod 777 cleanup.sh', shell=True)
    subprocess.call(['helm', 'repo', 'index', CHARTS_DIR])
    download_server_image = ''
    for i in docker_images_list:
        print(i)
        if isinstance(i,str):
            if "alaudaorg/download-server" in i:
                download_server_image = i
    r_message = '  启动helm download-server kaldr 容器'
    send_message(r_message)
    run_download_command = 'docker run -d --name downloadserver ' + download_server_image
    run_download = subprocess.Popen(run_download_command, shell=True)
    run_download.wait()
    subprocess.call('docker cp downloadserver:/download/ake .', shell=True)
    r_message = '  从download-server 容器内拷贝 ake '
    send_message(r_message)
    kaldr_image = ''
    subprocess.call('chmod 777 ake', shell=True)
    for i in docker_images_list:
        if isinstance(i,str):
            if "alaudaorg/kaldr" in i:
                kaldr_image = i
    run_kaldr_command = 'docker run --name acp-kaldr -d ' + kaldr_image
    run_kaldr = subprocess.Popen(run_kaldr_command, shell=True)
    run_kaldr.wait()
    subprocess.call('docker cp acp-kaldr:/kaldr kaldr', shell=True)
    r_message = '  从kaldr 容器内拷贝 软件源给 init 节点使用'
    send_message(r_message)
    rm_container('acp-kaldr')
    send_message(r_message)
    rm_container('registry')
    rm_container('chartmuseum')
    rm_container('downloadserver')

