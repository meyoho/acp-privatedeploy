# -*- coding: utf-8 -*-
# 下面的列表，分别是 acp、asm、aml、amp 这四个产品里的 chart list，这个 chart list 只包括 chart 内定义有镜像，需要在打包的时候，将这项镜像打到安装包里的 chart
ACPLIST = [
    'alauda-alb2',
    'alauda-base',
    'alauda-cluster-base',
    'alauda-container-platform',
    'alauda-log-agent',
    'ares',
    'artemis-e2e',
    'cert-manager',
    'cpaas-fit',
    'cpaas-lander',
    'cpaas-monitor',
    'cron-hpa-controller',
    'dashboard',
    'dex',
    'elasticsearch',
    'helloworld',
    'kafka-zookeeper',
    'kube-prometheus',
    'kubefed',
    'nfs-client-provisioner',
    'nfs-server-provisioner',
    'nginx-ingress',
    'prometheus-operator',
    'public-chart-repo',
    'sentry'
]
DEVOPSLIST = [
    'alauda-devops',
    'gitlab-ce',
    'harbor',
    'jenkins',
    'nexus',
    'sonarqube'
]
AMLLIST = [
    'aml-ambassador',
    'aml-core',
    'global-aml',
    'volcano'
]
AMPLIST = [
    'amp',
    'amp-kong'
]
ASMLIST = [
    'cluster-asm',
    'asm-demo',
    'asm-init',
    'global-asm',
    'istio',
    'istio-init',
    'jaeger-operator'
]
TDSQLLIST = [
    'tdsql'
]
CSPLIST = [
    'csp'
]
TILIST = [
    'hostdev',
    'ti-matrix', 
    'ti-matrix-installer'
]
TSFLIST = [
    'cpaas-tsf',
    'tsf-init',
    'tsf-region'
]
