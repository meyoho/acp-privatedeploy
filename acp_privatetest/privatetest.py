# -*- coding: utf-8 -*-
from acp_privatetest.common_func import assert_successful, remote_exec, check_cluster
from acp_privatetest.qcloud import create_instance, describe_instance, bind_lb
import subprocess
import settings
import sys
from time import sleep, strftime




def deployandtest(test_need_info):
    package_path = test_need_info["package_path"]
    print('1分钟后开始测试')
    sleep(60)
    print('开始启动腾讯机器')
    res_create = create_instance(3)
    assert_successful(res_create)
    res_describe = describe_instance()
    assert_successful(res_describe)
    ips = res_describe['ips']
    master_ip = ips[0]

    print('开始拷贝安装包')
    result = subprocess.getstatusoutput("sshpass -p {} scp -o StrictHostKeyChecking=no {} {}@{}:/{}/".format(
        settings.SSH_PASSWORD, package_path, settings.USER, master_ip, settings.USER))
    if result[0] != 0:
        return {"success": False, "message": "拷贝安装包到腾讯机器失败"}
    print('开始解压缩文件')
    tarfile_path = "/{}/{}".format(settings.USER, package_path.split('/')[-1])
    status, output = remote_exec(master_ip, "tar -zxvf {}".format(tarfile_path))
    if status != 0:
        return {"success": False, "message": "解压缩文件失败{}".format(output)}
    package_path = "/{}/alauda-k8s".format(settings.USER)
    pwd_path = sys.path[0]
    sleep(3)
    for ip in ips:
        status, output = subprocess.getstatusoutput(
            "sshpass -p {} scp -o StrictHostKeyChecking=no "
            "{path}/cleanup.sh {path}/generate_ca.sh {}@{}:/{}/".format(
                settings.SSH_PASSWORD, settings.USER, ip, settings.USER, path=pwd_path))
        if status != 0:
            return {"success": False, "message": "拷贝cleanup,generate_ca等脚本失败:{}".format(output)}
        cmd = "mkfs.xfs -n ftype=1 /dev/vdb;mkdir /var/lib/docker/;mount /dev/vdb /var/lib/docker;echo \"/dev/vdb /var/lib/docker xfs defaults 0 0\" >> /etc/fstab"
        status, output = remote_exec(ip, cmd)
        if status != 0:
            return {"success": False, "message": "挂盘失败{}".format(output)}

    print("开始部署macvlan集群")
    deploy_acp(ips, package_path, "macvlan")

    print("开始部署calico集群")
    deploy_acp(ips, package_path, "calico")

    print("开始部署非高可用模式vxlan集群")
    deploy_acp(ips, package_path, "vxlan")
    print("创建license以后才能用哦，真是麻烦。。")
    result = subprocess.getstatusoutput("kubectl create -f {}/license_key.yaml".format(pwd_path))
    if result[0] != 0:
        return {"success": False, "message": "天哪！创建个license也能失败！！！"}
    print("license 创建完成后要睡一分钟才能用哦，不要心急")
    sleep(60)

    return test_acp(master_ip, test_need_info)

    # print("开始部署高可用模式")
    # ret_bind = bind_lb()
    # assert_successful(ret_bind)
    # deploy_acp(ips, package_path, True)
    # print("创建license以后才能用哦，真是麻烦。。")
    # result = subprocess.getstatusoutput("kubectl create -f {}/license_key.yaml".format(pwd_path))
    # if result[0] != 0:
    #     return print("天哪！创建个license也能失败！！！:{}".format(result))
    # print("license 创建完成后要睡一分钟才能用哦，不要心急")
    # sleep(60)
    #
    # test_acp(settings.LB_IP, test_tags)


def deploy_acp(ips, package_path, type="vxlan"):
    master_ip = ips[0]
    slave_ip = ";".join(ips[1:])
    for i in range(0, len(ips)):
        remote_exec(ips[i], "bash /{}/cleanup.sh".format(settings.USER))
        remote_exec(ips[i], "hostname node{}".format(i))
    print('开始初始化操作')
    status, output = remote_exec(master_ip, "cd {};./ACP.init.sh -s --network-interface=eth0".format(package_path))
    print("初始化结果:{}".format(output))
    if status != 0:
        return {"success": False, "message": "执行init脚本做初始化失败{}".format(output)}

    status, output = remote_exec(master_ip, "yum install -y sshpass")
    if status != 0:
        return {"success": False, "message": "部署失败:{}".format(output)}
    if type == "vxlan":
        status, output = remote_exec(master_ip, "bash /{}/generate_ca.sh {}".format(settings.USER, master_ip))
        if status != 0:
            return {"success": False, "message": "生成key失败"}
        deploy_cmd = "{path}/ake deploy --pkg-repo=http://{master_ip}:7000/ --registry={master_ip}:60080 " \
                     "--chart_stable_repo_addr=http://chartmuseum:chartmuseum@{master_ip}:8088 " \
                     "--alauda_chart_repo=http://chartmuseum:chartmuseum@{master_ip}:8088/ --dockercfg=/etc/docker/daemon.json " \
                     "--masters={master_ip} --nodes=\"{master_ip};{slave_ip}\" --cert-sans={master_ip} --acp-host={master_ip} " \
                     "--network-opt=backend_type=vxlan --kube-pods-subnet=192.168.0.0/16  -su=root -sp={password} " \
                     "--enabled-features=helm,alauda-ingress,alauda-portal,alauda-dashboard,alauda-devops," \
                     "alauda-base,alauda-oidc,alauda-appcore --oidc_issuer_url=https://{master_ip}/dex " \
                     "--oidc_ca_file=/{home}/ssl/cert.pem" \
                     " --oidc_ca_key=/{home}/ssl/key.pem".format(path=package_path, master_ip=master_ip,
                                                                 password=settings.SSH_PASSWORD,
                                                                 slave_ip=slave_ip, home=settings.USER)
        print("目前会卖萌的机器人部署的是非高可用模式vxlan集群:一个master/slave一个slave，部署命令是:{}".format(deploy_cmd))
    elif type == "macvlan":
        deploy_cmd = "{path}/ake deploy --pkg-repo=http://{master_ip}:7000/ --registry={master_ip}:60080 " \
                     "--chart_stable_repo_addr=http://chartmuseum:chartmuseum@{master_ip}:8088 " \
                     "--alauda_chart_repo=http://chartmuseum:chartmuseum@{master_ip}:8088/ --dockercfg=/etc/docker/daemon.json " \
                     "--masters={master_ip} --nodes=\"{master_ip}\" --cert-sans={master_ip} " \
                     "--network=none --kube-pods-subnet=172.30.0.0/16  -su=root -sp={password} " \
                     "--enabled-features=alauda-monkey --monkey_gateway=172.30.0.1 --monkey_subnet_name=mymonkey " \
                     "--monkey_subnet_range_start=172.30.15.1 --monkey_subnet_range_end=172.30.15.25 --monkey_driver=empty " \
                     "--monkey_network_type=macvlan --proxy_mode=iptables".format(path=package_path,
                                                                                  master_ip=master_ip,
                                                                                  password=settings.SSH_PASSWORD)
        print("目前会卖萌的机器人部署的是macvlan集群:一个master/slave，部署命令是:{}".format(deploy_cmd))
    elif type == "calico":
        status, output = remote_exec(master_ip, "bash /{}/generate_ca.sh {}".format(settings.USER, master_ip))
        if status != 0:
            return {"success": False, "message": "生成key失败"}
        deploy_cmd = "{path}/ake deploy --pkg-repo=http://{master_ip}:7000/ --registry={master_ip}:60080 " \
                     "--chart_stable_repo_addr=http://chartmuseum:chartmuseum@{master_ip}:8088 " \
                     "--alauda_chart_repo=http://chartmuseum:chartmuseum@{master_ip}:8088/ --dockercfg=/etc/docker/daemon.json " \
                     "--masters={master_ip} --nodes=\"{master_ip};{slave_ip}\" --cert-sans={master_ip} --acp-host={master_ip} " \
                     "--network=alauda-calico --ipip_mode=Always --kube-pods-subnet=192.168.0.0/16  -su=root -sp={password} " \
                     "--enabled-features=helm,alauda-ingress,alauda-portal,alauda-dashboard,alauda-devops," \
                     "alauda-base,alauda-oidc,alauda-appcore --oidc_issuer_url=https://{master_ip}/dex " \
                     "--oidc_ca_file=/{home}/ssl/cert.pem" \
                     " --oidc_ca_key=/{home}/ssl/key.pem".format(path=package_path, master_ip=master_ip,
                                                                 password=settings.SSH_PASSWORD,
                                                                 slave_ip=slave_ip, home=settings.USER)
        print("目前会卖萌的机器人部署的是calico集群:一个master/slave一个slave，部署命令是:{}".format(deploy_cmd))
    else:
        return {"success": False, "message": "输入的部署类型有误:{}".format(type)}
    # else:
    #     status, output = remote_exec(master_ip, "bash /{}/generate_ca.sh {}".format(settings.USER, settings.LB_IP))
    #     if status != 0:
    #         return print("生成key失败")
    #     deploy_cmd = "{path}/ake deploy --pkg-repo=http://{master_ip}:7000/ --registry={master_ip}:60080 " \
    #                  "--chart_stable_repo_addr=http://chartmuseum:chartmuseum@{master_ip}:8088 " \
    #                  "--alauda_chart_repo=http://chartmuseum:chartmuseum@{master_ip}:8088/ --dockercfg=/etc/docker/daemon.json " \
    #                  "--masters=\"{master_ip};{slave_ip}\" --etcds=\"{master_ip};{slave_ip}\" --nodes=\"{master_ip};{slave_ip}\"" \
    #                  " --cert-sans={lb_ip} --acp-host={lb_ip} --kube_controlplane_endpoint={lb_ip} " \
    #                  "--network-opt=backend_type=vxlan --network-opt=cidr=192.168.0.0/16  -su=root -sp={password} " \
    #                  "--enabled-features=helm,alauda-ingress,alauda-portal,alauda-dashboard,alauda-devops," \
    #                  "alauda-base,alauda-oidc --oidc_issuer_url=https://{lb_ip}/dex" \
    #                  " --oidc_ca_file=/{home}/ssl/cert.pem" \
    #                  " --oidc_ca_key=/{home}/ssl/key.pem".format(path=package_path, master_ip=master_ip,
    #                                                              password=settings.SSH_PASSWORD, slave_ip=slave_ip,
    #                                                              lb_ip=settings.LB_IP, home=settings.USER)
    #     print("目前会卖萌的机器人部署的是高可用模式:三个master/slave，部署命令是:{}".format(deploy_cmd))
    status, output = remote_exec(master_ip, deploy_cmd)
    print("部署结果:{}".format(output))
    if status != 0:
        return {"success": False, "message": "部署失败:{}".format(output)}
    print("部署成功")

    print("开始拷贝k8s配置文件")
    copy_cmd = "sshpass -p {} scp -o StrictHostKeyChecking=no {}@{}:/etc/kubernetes/admin.conf /{}/.kube/config".format(
        settings.SSH_PASSWORD, settings.USER, master_ip, settings.USER)
    result = subprocess.getstatusoutput(copy_cmd)
    if result[0] != 0:
        return {"success": False, "message": "拷贝k8s配置文件失败:{}".format(output)}
    # result = check_cluster()
    # assert_successful(result)
    sleep(200)
    # 200s 差不多pod能全部运行 检查状态时获取字段一直出错

    # print("开始拷贝个测试镜像到集群内部，内网真是各种拷贝")
    # c = docker.from_env()
    # c.pull("index.alauda.cn/alaudaorg/qaimages:helloworld")
    # c.tag("index.alauda.cn/alaudaorg/qaimages:helloworld", "{}:60080/alaudaorg/qaimages:helloworld".format(master_ip))
    # status, output = subprocess.getstatusoutput(
    #     "docker save -o helloworld.tar {}:60080/alaudaorg/qaimages:helloworld".format(master_ip))
    # if status != 0:
    #     print("docker save image failed:{}".format(output))
    # status, output = subprocess.getstatusoutput(
    #     "sshpass -p {} scp -o StrictHostKeyChecking=no helloworld.tar {}@{}:/{}/".format(settings.SSH_PASSWORD,
    #                                                                                      settings.USER, master_ip,
    #                                                                                      settings.USER))
    # if status != 0:
    #     print("拷贝测试镜像失败:{}".format(output))
    # remote_exec(master_ip, "docker load -i /{}/helloworld.tar".format(settings.USER))
    # remote_exec(master_ip, "docker push {}:60080/alaudaorg/qaimages:helloworld".format(master_ip))


def test_acp(master_ip, test_tags):
    print("开始测试啦~")
    print("先测试link，这个可能耗费时间比较长哦")
    test_result = {"success": True}
    link_report_path = "/tmp/link-{}/".format(strftime("%Y%m%d"))
    link_cmd = "docker run -t -v {path}:/tmp/results/ -v /dev/shm:/dev/shm -v " \
               "/{user}/.kube/config:/{user}/.kube/config -e " \
               "BASE_URL=https://{master_ip}/console -e IMAGE={master_ip}:60080/alaudaorg/qaimages:helloworld" \
               " -e OIDC_ENABLED=true " \
               "index.alauda.cn/alaudak8s/link-tests:{link_tag}".format(path=link_report_path, user=settings.USER, \
                                                                        master_ip=master_ip, link_tag=test_tags["link"])

    result = subprocess.getstatusoutput(link_cmd)
    if result[0] != 0:
        test_result.update({"success": False, "link_result": "link 测试失败,查看路径{}下的报告".format(link_report_path)})
    else:
        test_result.update({"link_result": "link测试通过了"})

    print("再测试portal,link测试结果先保密")
    portal_report_path = "/tmp/portal-{}/".format(strftime("%Y%m%d"))
    portal_cmd = "docker run -t -v {path}:/tmp/results/ -v /dev/shm:/dev/shm -v " \
                 "/{user}/.kube/config:/{user}/.kube/config -e OIDC_ENABLED=true -e CASE_TYPE=fulltest -e " \
                 "BASE_URL=https://{master_ip}/ -e IMAGE={master_ip}:60080/alaudaorg/qaimages:helloworld " \
                 "index.alauda.cn/alaudak8s/portal-tests:{portal_tag}".format(path=portal_report_path,
                                                                              user=settings.USER,
                                                                              master_ip=master_ip,
                                                                              portal_tag=test_tags['portal'])

    result = subprocess.getstatusoutput(portal_cmd)
    if result[0] != 0:
        test_result.update({"success": False, "portal_result": "portal 测试失败,查看路径{}下的报告".format(portal_report_path)})
    else:
        test_result.update({"portal_result": "portal测试通过了"})

    print("测试devops-api")
    devopsapi_report_path = "/tmp/devops-api-{}/".format(strftime("%Y%m%d"))
    devopsapi_cmd = "docker run -t -v {path}:/tmp/results/  -v /{}/.kube/config:/root/.kube/config" \
                    " index.alauda.cn/alaudak8s/devops-apiserver-tests:{}".format(settings.USER,
                                                                                  test_tags["devops-apiserver"],
                                                                                  path=devopsapi_report_path)

    result = subprocess.getstatusoutput(devopsapi_cmd)
    if result[0] != 0:
        test_result.update(
            {"success": False, "devopsapi_result": "devopsapi 测试失败,查看路径{}下的报告".format(devopsapi_report_path)})
    else:
        test_result.update({"devopsapi_result": "devopsapi测试通过了"})

    print("最后测试appcore/diablo")
    diablo_report_path = "/tmp/diablo-{}/".format(strftime("%Y%m%d"))
    diablo_cmd = "docker run -t -v {path}:/tmp/results/ -v /dev/shm:/dev/shm -v " \
                 "/{user}/.kube/config:/root/.kube/config -e " \
                 "BASE_URL=https://{master_ip}/devops/ -e TESTIMAGE={master_ip}:60080/alaudaorg/qaimages:helloworld" \
                 " -e OIDC_ENABLED=true -e CASE_TYPE=appcore " \
                 "index.alauda.cn/alaudak8s/diablo-e2e:{diablo_tag}".format(path=diablo_report_path, user=settings.USER, \
                                                                            master_ip=master_ip,
                                                                            diablo_tag=test_tags["diablo"])

    result = subprocess.getstatusoutput(diablo_cmd)
    if result[0] != 0:
        test_result.update({"success": False, "diablo_result": "diablo 测试失败,查看路径{}下的报告".format(diablo_report_path)})
    else:
        test_result.update({"diablo_result": "diablo测试通过了"})

    print("测试结束了，请看结果:{}".format(str(test_result)))
    return test_result
