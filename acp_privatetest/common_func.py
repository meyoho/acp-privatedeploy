# coding=utf-8
import json
from subprocess import getstatusoutput
from time import sleep
import settings


def assert_successful(response):
    msg = response.get('message', json.dumps(response))
    if not response["success"]:
        print(msg=msg)
    assert response["success"], msg


def remote_exec(ip, cmd):
    commands = "sshpass -p {} ssh -o StrictHostKeyChecking=no {}@{} '{}'".format(settings.SSH_PASSWORD, settings.USER,
                                                                               ip, cmd)
    print("excute cmd :{}".format(commands))
    status, output = getstatusoutput(commands)
    if status != 0:
        print("excute output :{}".format(output))
    return status, output


def check_cluster():
    cnt = 0
    flag = False
    message = "kubernetes pods deploy failed"
    while cnt < 60 and not flag:
        try:
            cnt += 1
            status, output = getstatusoutput("kubectl get pod --all-namespaces|grep -v Running |wc -l")
            if status != 0:
                return {"success": False, "message": "kubectl get pod failed: {}".format(output)}
            print(output)
            if output and int(output) > 1:
                sleep(5)
                message = "kubernetes pod  deploy timeout"
                break
            else:
                flag = True
                message = "kubernetes pods deploy success"
        except Exception as e:
            print(e)
            continue
    return {"success": flag, "message": message}
