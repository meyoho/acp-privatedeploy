from time import sleep, time
from QcloudApi.qcloudapi import QcloudApi
import random
import base64
import hashlib
import hmac
import requests
import settings
import os

secret_id = settings.SECRET_ID
secret_key = settings.SECRET_KEY
endpoint = "cvm.tencentcloudapi.com"
instances_id = []
vm_path= os.getcwd() + "/vm_list"


def get_string_to_sign(method, endpoint, params):
    s = method + endpoint + "/?"
    query_str = "&".join("%s=%s" % (k, params[k]) for k in sorted(params))
    return s + query_str


def sign_str(key, s, method):
    key = str(base64.b64decode(key), encoding="utf-8")
    hmac_str = hmac.new(key.encode("utf8"), s.encode("utf8"), method).digest()
    return base64.b64encode(hmac_str)


def create_instance(num):
    params = {
        "Action": "RunInstances",
        "Placement.Zone": "ap-chongqing-1",
        "Placement.ProjectId": 1126842,
        "Region": "ap-chongqing",
        "ImageId": "img-8toqc6s3",
        "InstanceType": "S3.LARGE8",
        "InstanceCount": num,
        "SystemDisk.DiskType": "CLOUD_PREMIUM",
        "SystemDisk.DiskSize": 50,
        "DataDisks.0.DiskType": "CLOUD_PREMIUM",
        "DataDisks.0.DiskSize": 50,
        "InstanceName": "AcpPrivateDeploy-DT",
        "LoginSettings.Password": settings.SSH_PASSWORD,
        "SecurityGroupIds.0": "sg-f9whhvbu",
        "Version": "2017-03-12",
        "HostName": "node",
        "InternetAccessible.InternetChargeType": "TRAFFIC_POSTPAID_BY_HOUR",
        "InternetAccessible.InternetMaxBandwidthOut": 1,
        "InternetAccessible.PublicIpAssigned": True,
        "TagSpecification.0.ResourceType": "instance",
        "TagSpecification.0.Tags.0.Key": "Group",
        "TagSpecification.0.Tags.0.Value": "QA",
        "Timestamp": int(time()),
        "Nonce": random.randint(0, 1000),
        "SecretId": secret_id
    }
    s = get_string_to_sign("GET", endpoint, params)
    params["Signature"] = sign_str(secret_key, s, hashlib.sha1)
    res = requests.get("https://" + endpoint, params=params)
    global instances_id
    instances_id = res.json()['Response'].get("InstanceIdSet")
    with open(vm_path, 'w') as f:
        f.write(";".join(instances_id))
    if not instances_id:
        return {"success": False, "message": "create qcloud vm failed: {}".format(res.text)}
    sleep(120)
    return {"success": True, "instances_id": instances_id, "message": "create qcloud vm over"}


def describe_instance():
    params = {
        "Action": "DescribeInstances",
        "Version": "2017-03-12",
        "Region": "ap-chongqing",
        "SecretId": secret_id,
        "Timestamp": int(time()),
        "Nonce": random.randint(0, 1000),
    }
    for i in range(0, len(instances_id)):
        params.update({"InstanceIds." + str(i): str(instances_id[i])})
    s = get_string_to_sign("GET", endpoint, params)
    params["Signature"] = sign_str(secret_key, s, hashlib.sha1)
    res = requests.get("https://" + endpoint, params=params)
    if "InstanceSet" in res.json()['Response']:
        private_ips = []
        instances_info = res.json()['Response']['InstanceSet']
        for instance_info in instances_info:
            private_ips.append(str(instance_info['PrivateIpAddresses'][0]))
        return {"success": True, "ips": private_ips, "message": "describe qcloud vm over"}
    return {"success": False, "message": "get qcloud vm info failed:{}".format(res.text)}


def delete_instance():
    params = {
        "Action": "TerminateInstances",
        "Version": "2017-03-12",
        "Region": "ap-chongqing",
        "SecretId": secret_id,
        "Timestamp": int(time()),
        "Nonce": random.randint(0, 1000),
    }
    if os.path.exists(vm_path):
        with open(vm_path, 'r') as f:
            instances_id = f.read().split(";")
    for i in range(0, len(instances_id)):
        params.update({"InstanceIds." + str(i): str(instances_id[i])})
    s = get_string_to_sign("GET", endpoint, params)
    params["Signature"] = sign_str(secret_key, s, hashlib.sha1)
    res = requests.get("https://" + endpoint, params=params)
    if "RequestId" not in res.json()['Response']:
        return {"success": False, "message": "delete qcloud vm failed,please check it"}
    return {"success": True, "message": "delete qcloud vm over"}


def bind_lb():
    module = 'lb'
    action = 'RegisterInstancesWithForwardLBFourthListener'
    config = {
        'Region': 'ap-chongqing',
        'secretId': secret_id,
        'secretKey': str(base64.b64decode(secret_key), encoding="utf-8"),
        'method': 'GET',
        'SignatureMethod': 'HmacSHA1',
        'Version': '2017-03-12'
    }
    service = QcloudApi(module, config)
    action_params = {
        "projectId": "1126842",
        "forward": 1,
        "loadBalancerId": "lb-a0nql363",
        "listenerId": "lbl-20d0242p"
    }
    for i in range(0, len(instances_id)):
        action_params.update({"backends.{}.instanceId".format(i): str(instances_id[i])})
        action_params.update({"backends.{}.port".format(i): 6443})
    if "Success" not in str(service.call(action, action_params)):
        return {"success": False, "message": "bind lb failed"}
    sleep(10)
    action_params.update({"listenerId": "lbl-2j1jjwrt"})
    for i in range(0, len(instances_id)):
        action_params.update({"backends.{}.port".format(i): 443})
    if "Success" not in str(service.call(action, action_params)):
        return {"success": False, "message": "bind lb failed"}
    return {"success": True, "message": "bind lb success"}
