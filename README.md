# acp-privatedeploy

> 小平台自动打包机器人，产出供私有部署使用的安装包。

## 系统依赖

1. docker
2. helm（下载二进制文件放入PATH即可）
3. 运行过程中会占用 25000 和 28000 两个端口
4. 目前需要大约10G的存储空间及9G左右的docker数据空间
5. 代码为Python3，建议使用venv提供运行环境

## 工作流程

1. 自定义工作目录
2. 从chartmuseum获取所有charts
3. 解压chart.tgz后从value.yaml中解析依赖的docker镜像
4. 推送所有镜像到本地仓库
5. 生成chartmuseum需要的index.yaml
6. 下载最新版本的ake
7. 从本项目获取私有部署需要的其他脚本
8. 打包
9. [可选] 自动部署到测试环境
10. 日志输出和钉钉通知可自定义

