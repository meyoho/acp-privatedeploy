#!/bin/bash
dir=$(pwd)
num=$(kubectl get node  --show-labels  | grep -i 'node-role.kubernetes.io/master=' | awk '{print $1}' | wc -l)
if [ $num -ne 3 ]
then
	echo "please check it's a high availability cluster"
	exit
else
	for i in $(kubectl get node  --show-labels  | grep -i 'node-role.kubernetes.io/master=' | awk '{print $1}')
	do
		kubectl label node $i soul=pure --overwrite
		kubectl taint node $i soul=pure:NoSchedule
	done
fi
for i in $(cat $dir/component.txt)
do
	kubectl get deploy $i -n alauda-system -o yaml > $dir/deploy/$i.yaml
	sed -i "/dnsPolicy/r $dir/tolerations.yaml" $dir/deploy/$i.yaml
	kubectl replace -f $dir/deploy/$i.yaml
done
